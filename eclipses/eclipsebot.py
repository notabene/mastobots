# coding=utf-8

from mastodon import Mastodon
from time import *
from datetime import datetime, timedelta
import sys

sys.path.insert(0, '..')
from botinit import get_api
import gen

current_directory = sys.argv[1]

api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")

dernier_notif = api.notifications()[0]

# Get things done
edate, etype = gen.get_next()

while True:
    '''# Toot if an eclipse is soone
    toot_text = toot()
    api.status_post(
        toot_text,
        visibility='public'
    )'''

    # Reply to mentions
    latest_mentions = [e for e in api.notifications(since_id=dernier_notif)
                       if e.type=='mention']
    
    for mention in latest_mentions:
        toot_text = "@" + str(mention.account.acct) + " " + gen.toot(edate, etype)

        api.status_post(
            toot_text,
            in_reply_to_id = mention.status.id,
            visibility = mention.status.visibility
        )
        dernier_notif = mention

    # Say if an eclipse is comming up
    today = datetime.now()
    if (edate - today).days == 7:
        toot_text = "Il y aura une éclipse lunaire dans une semaine. Plus d'informations sur ce qu'est une éclipse lunaire : https://fr.wikipedia.org/wiki/%C3%89clipse_lunaire"
        
        api.status_post(
            toot_text,
            visibility = "public"
        )
        
    if (edate - today).days == 1:
        toot_text = "La prochaine éclipse est demain. Ce sera une éclipse {}. Plus de détails sont disponibles ici https://promenade.imcce.fr/fr/pages3/334.html".format(etype)
        
        api.status_post(
            toot_text,
            visibility = "public"
        )
 
    if (edate - today).days == 0:
        edate, etype = gen.get_next()
        toot_text = "L'éclipse est aujourd'hui. La suivante sera le {}/{}/{}. Ce sera une éclipse {}".format(edate.day, edate.month,edate.year, etype)
        
        api.status_post(
            toot_text,
            visibility = "public"
        )
    
    
    # Sleep
    sleepy_time = 43200 # check twice a day in seconds
    sleep(sleepy_time)
