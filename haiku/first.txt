sur une branche morte
premier jour de l’An —
les fleurs de quel arbre —
au milieu du champ
mon père et ma mère
un vieil étang
réveille-toi, réveille-toi
avec chaque souffle
Au cours de sa chute
la cloche se tait
tant et tant de choses
dans les pluies de mai
le printemps s'en va
devant un éclair
Ah ! le coucou
Ah ! tranquillité —
temple de Suma
cet automne-ci
L’automne profond —
ce chemin-ci
la rosée blanche
ah ! belle-de-jour
cette solitude
voyageur
ami, allume le feu
désolation hivernale
fixé nulle part
reclus pour l’hiver-
la première pluie-
ma cruche éclatant
malade en voyage
début de l’automne
ô pluie du printemps
à cheval assoupi
le vent d'automne
on rallonge
Au printemps qui s'en va
Au nectar d'orchidée
Sur le sentier de montagne
Nuit d'été
Au fond de la jarre
Herbes d'iris
Ce chemin —
Dans le goût mordant du radis
Le saule s'effeuille —
Poireaux lavés
La nuit tombe sur la mer —
les fleurs des théiers
le poirier en fleurs
avec le pied
venant les regarder à nouveau
nuit courte
la pivoine coupée
quiétude
tombés de la pivoine
dans le vieux puits
à l’aube
la rumeur des moustiques
soirée de pleine lune
dans le champ envahi par les ronces
dans mon jardin
s’apprêtant à couper
m’arrêtant
la lune voilée
la bourrasque de la montagne
un éclair
pas une feuille ne bouge
poêles et casseroles
par la pauvreté
au fond de la source chaude
un seau sans fond
assoupi
près des pieds
la goutte au nez
les graffiti sur le mur
matin de neige
neige au crépuscule
le bruit des branches qui cassent sous la neige
je vais me coucher
dans le soir sombre
la journée s’étire-
la lune brumeuse-
montant jusqu’au ciel
ondée printanière-
nul pont par ici
pluie de printemps-
les lentes journées
loin de la maison
sur la grosse cloche
Les fleurs de prunier
Chaque fleur qui tombe
Les travaux des champs —
azalées en fleur
la mer au printemps
Ah ! le cerf-volant
En tombant dans l’eau
ah ! le rossignol
Ah ! le rossignol
à la nuit tombante
avalant des nuages
venant vers les fleurs
Ah ! fleurs de colza
le manteau de paille
hier a pris fin
la fin du printemps —
chandelle à la main
C’est le dernier jour
Ah ! quelle fraîcheur
Quelle courte nuit !
Averses d’été-
fourmi de montagne
Rivière d’été-
éclairs de chaleur —
ah ! la pauvreté
au son de la flûte
auprès du poirier
Ah ! le batelier
Il brama trois fois
Le cerf, la montagne —
Ah ! le chrysanthème —
Une solitude
Renonçant au monde
Le mont s’assombrit
Le mont s’assombrit
Ah ! quelle douleur —
Quand ça souffle à l’ouest
Traversant l’assiette
Tous mes os
Ah ! la lune d’hiver —
sous la lune froide
En plantant la hache
Ah ! lune d’hiver —
un soir de pluie froide —
tombe la bruine —
dans le vieil étang
Temple de montagne —
Ah ! vent froid d’hiver –
Le soleil se couche
Rivière d’hiver —
Au bas de ma robe
Bashô parti—
Retiré l’hiver
fleur de camélia
Le propriétaire du champ
Nuit de lune
De hautes vagues fouettent le sable
Dans la boîte à clous
Dans mon ermitage
Je suis bien malade
La colline au printemps
Le bruit du sécateur
Le poissonnier
de la mendiante
dans le regard d’un bœuf
disant adieu
au petit jour
sur la pointe d’une herbe
j’ai balayé les feuilles mortes
Dans la touffeur verte
Dans le vent d’automne
Le ciel d’automne
Tendre souvenir :
La fenêtre ouverte
Oh ! le toit qui fuit —
les jours de pluie
le nouvel étang
allons, c’est fini !
Ah ! Le rossignol
Ah ! Le rossignol —
Un très vieil homme
un iris
Cueillant des kakis
En venant je tape
Sur la branche encore
La haie de branchages
Brûlant du bois mort
Sur ma porte de branchages
Sa verte fraîcheur
Le vent de l’été
Au petit matin
Pour faire le feu
Là elle se couche
Prêtez bien l’oreille :
Pour me souvenir
Le riz pour demain
Un linge sur la tête
Ma main se lassant
Une nuit d’été
Lavant le chaudron
Le grèbe ! son nid —
Le voleur parti
Quel soulagement !
Quelle insouciance !
Trèfle et herbe d’automne —
Trèfle et herbe d’automne
Tapant retapant
La pluie de printemps —
La pluie de printemps —
Quelqu’un est venu
C’est ainsi pour tous :
Des jours et des jours
Ramassant du bois
Ah ! Le liseron
L’heure de midi —
La clochette à vent
Un fleuve en hiver
Ah ! si tout le jour
Comme par ivresse
Le sommet atteint
À la mi-journée
La surface de l’eau
À la pleine lune
À la pleine lune
L’automne – un brocard
Réparant le toit
Un hameau de montagne —
La montagne en fleurs
Un calme parfait
La neige fondant
L’automne prend fin —
Sortant de mes rêves
Où donc m’assoupir
La tombée du jour —
Quel plaisir ! dormir
Tout autour de nous
Triste et solitaire
L’amour quant à moi
Jusqu’à ma cabane
J’entends les cris
Ah le rossignol
hirondelles du soir
dans les fleurs de thé
A l’ombre des fleurs
Ah le papillon
ne possédant rien
de nouveaux habits :
un homme tout seul
ver, ne pleure pas !
avec un tel chant
ce matin sans doute
viens donc avec moi
chassant une mouche
du premier baquet
lorsqu’on vieillit
mon ombre elle aussi
temple de montagne
mes amis sous les fleurs
fleurs de nénuphar
petit coucou
ah, sérénité
d’un pet de cheval
au dehors la neige
nuages aux oiseaux
dans le caniveau
me suis retourné
ô sérénité
quand ne serai plus
rosée sur ma coiffe
tant me rendent confus
essuyant ma sueur
au vent de la plage
auprès du foyer
auprès de mes pieds
du corps sur sa couche
et nuit après nuit
aux ombres du soir
resté en ce monde
une soirée sans lune
bourrasque de vent
et pruniers en fleur
oies sauvages s’en vont
cerisiers du soir
l’escargot
printemps au foyer
tranquillité
dormant sur la pierre papillon–
comme compagnon
dans l’eau de la cruche
la chauve-souris
cessant de crier
au coin d’un vieux mur
un banc de truites
la barque et la rive juste
le clapotis
ah la longue nuit
Ah le vent d’automne
les jours qui me restent
au seuil de la mort
retraite d’hiver
rosée que ce monde
sous la brise d'automne
vapeur qui s'élève de la terre
un cerf-volant flotte
soir du printemps
courte nuit d'été
il reste éveillé
sur l'éventail
sommeil sur le dos d'un cheval,
le printemps passe
le zashiki d'été
quel plaisir !
le vent d'automne
de tous les côtés
même un sanglier
le croissant éclaire
le lespédèze fleuri ondule
étang dégelé
quelle fraîcheur de l'air
des feuilles de lotus dans l'étang
de la fumée tourbillonne
la tempête
on ne voit pas la lune
au-dessus d'un creux de rocher
le luffa a fleuri
un serpent s'est enfui
on appelle cette fleur pivoine blanche
les filles prennent des plants de riz
un chien s'endort
je regarde la rivière
quand on dépose une chose
les racines d'un grand arbre d'été
j'ai reçu un pétale tombé de cerisier à la main
vient le premier papillon de l'année
la nourrice
c'est un vrai taudis
l'air frais d'automne
la pluie menace ­
le jour sur les fleurs
le chêne
pour un simple locataire
la cueillir quel dommage !
dans l'eau que je puise
quand le jardin
les fleurs de cerisiers tombées
point du jour
si tu es tendre pour eux
comme si rien n'avait eu lieu
de quel air revêche
davantage que celles de cerisiers
pousses de bambou
affalé au sol
emporte-moi
la rivière d'été
la fumée
ces fleurs de cerisier
le papillon bat des ailes
tremblant dans les herbes
par-dessus la mer
rien d'autre aujourd'hui
au clair de lune
le halo de la lune
dans les fleurs tardives du cerisier
cheminant par la vaste lande
la brise fraîche
sous les pluies d'été
il reste éveillé
claire lune automnale
on voit dans ses yeux
il est transi
foulant les feuilles dorées du ginkgo
les herbes se couvrent
sur une branche morte
une rafale de vent
ce chemin
feuille morte au vent
sur la feuille de lotus
appuyé contre l'arbre nu
on grille des châtaignes
un oiseau chanta
vent d'automne
on grille des châtaignes
la bruine d'hiver
dans le clair de lune glacé
la pluie d'hiver
ces mêmes montagnes
solitude hivernale
un oiseau s'envole
une baie rouge
