# coding=utf-8

from mastodon import Mastodon
from time import sleep
from random import choice
from datetime import datetime, timedelta
import pause
import sys

sys.path.insert(0, '..')
from botinit import get_api


current_directory = sys.argv[1]

api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")


# Get things done
input_file = current_directory + 'dictionnaire.txt'
with open(input_file, 'r') as f:
    def_list = f.read().splitlines()


#entries = {}
#for d in def_list:
#    k, v = parse(d)
#    entries[k] = v

while True:
    now = datetime.now()
    next_time = datetime(now.year, now.month, now.day, 10, 0, 0) + timedelta(days = 1)
    pause.until(next_time)
    
    definition = choice(def_list)
    try: 
        key, value = definition.split(':')
    except ValueError:
        tmp = definition.split(':')
        key = tmp[0]
        value = ' : '.join(tmp[1:])
    toot_text = key + " : \n \n" + value

    api.status_post(
        toot_text,
        visibility='public'
    )

