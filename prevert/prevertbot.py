# coding=utf-8

from mastodon import Mastodon
from time import *
from random import *
import sys
from botinit import get_api

current_directory = sys.argv[1]

api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")

# Get things done
input_file = current_directory + 'phrases.txt'
with open(input_file, 'r') as f:
    phrases = f.read().splitlines()

times = [3, 36, 360, 600, 1200]
toot_ids = []

while True:
    # Toot
    toot_text = choice(phrases)
    print(toot_text)
    tmp = api.status_post(
        toot_text,
        visibility='public'
    )

    # Delete old toots
    toot_ids.append(tmp.id)
    if len(toot_ids)>100:
        to_delete = toot_ids.pop(0)
        api.status_delete(to_delete)
    
    # Sleep
    tmp = choice(times)*randint(90,110)
    print(tmp)
    sleep(tmp)
